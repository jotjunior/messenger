Jot! Messenger
==============

Introdução
----------
Módulo desenvolvido para o Zend Framework 2 que facilita a troca de mensagens
entre usuários cadastrados no sistema.

O modulo, além de enviar as mensagens por e-mail, as grava em banco de dados, 
e se o arquivo `application.global.php` for corretamente configurado, uma imagem
de sua escolha é usada pela aplicação para marcar a mensagem como lida no banco
de dados.

Instalação
----------

Usando Composer (recomendado)
-----------------------------
Adicione as seguintes linhas no seu composer.json:

    {
        "require": {
            "jotjunior/messenger": "dev-master"
        },
        "repositories": [
            {  
                "type": "git",
                "url": "git@bitbucket.org:jotjunior/messenger.git"
            }
        ]
    }
   
Em seguida, use o comando `php composer.phar install`

Exemplo de uso
--------------
Use o método da seguinte maneira:

    $mail = $this->getServiceLocator()->get('Messenger')
                    ->setTemplate('default')
                    ->setData(array('message' => 'Texto da mensagem', 'variavel' => 'Valor))
                    ->setSubject('Assunto da mensagem')
                    ->setFrom(\Uacl\Entity\User $userFrom)
                    ->addTo(\Uacl\Entity\User $userTo)
                    ->saveMessage()
                    ->send();


Arquivo de configuração
-----------------------
Caso seja necessário envio de e-mails por autenticação por algum provedor, como
o google, por exemplo, crie um arquivo dentro de config/autload/seuload.global.php
com o seguinte conteúdo:

    <?php

    return array(
        'messenger' => array(
            'name' => 'smtp.gmail.com',
            'host' => 'smtp.gmail.com',
            'connection_class' => 'login',
            'port' => 587,
            'connection_config' => array(
                'username' => 'remetente@gmail.com',
                'password' => 'senha',
                'ssl' => 'tls',
                'from' => 'remetente@gmail.com',
            )
        )
    );

Tabelas de banco de dados
-------------------------
O diretório `dist` do pacote contém os exemplos dos arquivos de configuração,
além do scripts SQL para criação das tabelas de banco de dados.