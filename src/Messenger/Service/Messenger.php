<?php

namespace Messenger\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\Sendmail as SendmailTransport;
use Zend\Mail\Transport\SmtpOptions;
use Messenger\Courier\Mail;

class Messenger implements FactoryInterface {

	public function createService(ServiceLocatorInterface $serviceLocator) {
		// buscando os dados do config
		$config = $serviceLocator->get('config');

		// adicionando as opções de SMTP somente se elas forem setadas
		if (isset($config['messenger'])) {
			// instanciando smtp transport
			$transport = new SmtpTransport;

			// definindo as opcoes
			$options = new SmtpOptions($config['messenger']);

			// adicionando as opcoes ao transport
			$transport->setOptions($options);
		} else {
			// caso contrário, envia por sendmail
			$transport = new SendmailTransport();
		}

		// Guardando dados da aplicação
		$applicationData = $config['application'];

		// instanciando o Entity Manager
		$em = $serviceLocator->get('Doctrine\ORM\EntityManager');

		// buscando dados da view
		$view = $serviceLocator->get('view');

		return new Mail($em, $applicationData, $transport, $view);
	}

}
