<?php

namespace Messenger\Courier;

use Doctrine\ORM\EntityManager;

class Message {

	protected $em;
	protected $app;
	protected $subject;
	protected $to = array();
	protected $from;
	protected $data = array();
	protected $attachment = array();
	protected $imageLogo;
	protected $entityMessage;

	public function __construct(EntityManager $em, array $applicationData) {
		$this->em = $em;
		$this->app = $applicationData;
	}

	/**
	 * Define o assunto da mensagem
	 * @param string $subject
	 * @return \Messenger\Courier\Message
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
		return $this;
	}

	/**
	 * Informa o remetente
	 * @param \Uacl\Entity\User $from
	 * @return \Messenger\Courier\Message
	 */
	public function setFrom(\Uacl\Entity\User $from) {
		$this->from = $from;
		return $this;
	}

	/**
	 * Adiciona um destinatário
	 * @param \Uacl\Entity\User $to
	 * @return \Messenger\Courier\Message
	 */
	public function addTo(\Uacl\Entity\User $to) {
		array_push($this->to, $to);
		return $this;
	}

	/**
	 * 
	 * @param string $attachment
	 * @return \Messenger\Courier\Message
	 */
	public function addAttachment(array $attachment) {

		if (!isset($attachment['name'])) {
			throw new \InvalidArgumentException('Você deve informar o nome do anexo');
		}

		// verificando existência do arquivo
		if (!is_file($attachment['file'])) {
			throw new \InvalidArgumentException('O arquivo que você está tentando anexar não existe');
		}

		// sanitizando o arquivo para acesso por URL
		$attachment['uri'] = $this->normalizeAttachment($attachment['file']);

		// adicionando o arquivo à coleção 
		array_push($this->attachment, $attachment);

		return $this;
	}

	/**
	 * Variáveis da mensagem que serão aplicadas no template do e-mail
	 * @param array $data
	 * @return \Messenger\Courier\Message
	 */
	public function setData(array $data) {
		$this->data = $data;
		return $this;
	}

	public function saveMessage() {
		if (!count($this->to)) {
			throw new \Exception('Você deve informar ao menos um destinatário');
		}

		// gravando a mensagem no banco de dados
		$this->entityMessage = new \Messenger\Entity\Message;
		$this->entityMessage->setUser($this->from)
				->setSubject($this->subject)
				->setText($this->data['message']);
		$this->em->persist($this->entityMessage);

		// adicionando os destinatários
		foreach ($this->to as $to) {
			$recipient = new \Messenger\Entity\Recipient;
			$recipient->setMessage($this->entityMessage);
			$recipient->setUser($to);
			$this->em->persist($recipient);
		}

		// adicionando anexos, caso existam
		if (count($this->attachment)) {
			foreach ($this->attachment as $attachment) {
				$attachment = new \Messenger\Entity\Attachment(array(
					'name' => $attachment['name'],
					'uri' => $attachment['uri'],
					'message' => $this->entityMessage
				));
				$this->em->persist($attachment);
			}
		}
		$this->em->flush();

		return $this;
	}

	protected function normalizeAttachment($attachment) {
		return $this->app['url'] . str_replace('public', '', $attachment);
	}

}

