<?php

namespace Messenger\Courier;

use Zend\Mail\Transport\TransportInterface;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\View\Model\ViewModel;
use Doctrine\ORM\EntityManager;

class Mail extends \Messenger\Courier\Message {

	protected $transport;
	protected $view;
	protected $body;
	protected $message;
	protected $template = 'default';

	public function __construct(EntityManager $em, array $applicationData, TransportInterface $transport, $view) {
		parent::__construct($em, $applicationData);

		$this->transport = $transport;
		$this->view = $view;
	}

	/**
	 * Nome do arquivo de template da mensagem
	 * @param string $template
	 * @return \Messenger\Courier\Mail
	 */
	public function setTemplate($template) {
		$this->template = $template;
		return $this;
	}

	/**
	 * Renderiza a view para retornar a string completa do corpo da mensagem
	 * @param array $data
	 * @return string
	 */
	public function renderView() {
		$model = new ViewModel;
		$model->setTemplate("message/{$this->template}.phtml");
		$model->setOption('has_parent', true);
		$model->setVariables($this->data);

		return $this->view->render($model);
	}

	/**
	 * Envia a mensagem por e-mail
	 */
	public function send() {
		foreach ($this->to as $to) {
			// adicionando a string da logo que fará a leitura automática da mensagem pelo e-mail
			$this->data['user'] = $to;
			$this->data['imageLogo'] = $this->createImageLogoName($this->entityMessage->getId(), $to->getId());
			$this->data['url'] = $this->app['url'];
			
			// montando a mensagem a partir do template e dos dados
			$html = new MimePart($this->renderView($this->template, $this->data));
			$html->type = 'text/html';

			// montando o corpo da mensagem
			$body = new MimeMessage();
			$body->setParts(array($html));
			$this->body = $body;

			// instanciando o objeto de mensagem 
			$this->message = new \Zend\Mail\Message;
			$this->message->setSubject($this->subject)
					->setBody($this->body)
					->setFrom($this->from->getEmail(), $this->from->getFirstName());
			
			// adicionando o destinatário
			$this->message->addTo($to->getEmail(), $to->getFirstName());
			
			// enviando a mensagem
			$this->transport->send($this->message);
			
			// atualizando o texto da mensagem
			$this->entityMessage->setText($html->getContent());
			$this->em->persist($this->entityMessage);
			$this->em->flush();
		}
		
		// retorna a entidade com os dados da mensagem gerada
		return $this->entityMessage;
	}

	/**
	 * Gera um código base64 com os dados da mensagem e usuário para exibição da logo do site que aparecerá no e-mail
	 * @param int $this->entityMessageId
	 * @param int $userId
	 * @return string
	 */
	protected function createImageLogoName($messageId, $userId) {
		return str_replace('/', '-', str_replace('=', '_', base64_encode(serialize(array('messageId' => $messageId, 'userId' => $userId)))));
	}

}

