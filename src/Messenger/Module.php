<?php

/**
 * Módulo Base
 * Básico do sistema. Local onde fica o layout e classes que são sompartilhadas entre as demais
 *
 * @link      http://www.jot.com.br
 * @copyright Copyright (c) 2013 Jot (http://www.jot.com,br)
 * @author João G. "Jot!" Zanon Jr. <jot@jot.com.br>
 */

namespace Messenger;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Messenger' => 'Messenger\Service\Messenger',
            )
        );
    }

}
