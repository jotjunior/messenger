<?php

namespace Messenger\Entity;

use Doctrine\ORM\EntityRepository;

class RecipientRepository extends EntityRepository {

	public function countUnreadMessages(\Uacl\Entity\User $user) {
		return $this->createQueryBuilder('m')
						->select('COUNT(m) AS unread')
						->where('m.readed IS NULL')
						->andWhere('m.user = :user')
						->setParameter('user', $user)
						->getQuery()
						->getResult();
	}

}
