<?php

namespace Messenger\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MsgRecipients
 *
 * @ORM\Table(name="msg_recipients", indexes={@ORM\Index(name="message_id", columns={"message_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="RecipientRepository")
 */
class Recipient {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="readed", type="datetime", nullable=true)
	 */
	private $readed;

	/**
	 * @var boolean
	 *
	 * @ORM\Column(name="removed", type="boolean", nullable=false)
	 */
	private $removed;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="source", type="string", length=255, nullable=false)
	 */
	private $source;

	/**
	 * @var \MsgMessages
	 *
	 * @ORM\ManyToOne(targetEntity="Message")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="message_id", referencedColumnName="id")
	 * })
	 */
	private $message;

	/**
	 * @var \UaclUsers
	 *
	 * @ORM\ManyToOne(targetEntity="Uacl\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
	private $user;

	public function __construct() {
		$this->removed = 0;
	}

	public function getId() {
		return $this->id;
	}

	public function getReaded() {
		return $this->readed;
	}

	public function getRemoved() {
		return $this->removed;
	}

	public function getSource() {
		return $this->source;
	}

	public function getMessage() {
		return $this->message;
	}

	public function getUser() {
		return $this->user;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setReaded($readed) {
		$this->readed = $readed;
		return $this;
	}

	public function setRemoved($removed) {
		$this->removed = $removed;
		return $this;
	}

	public function setSource($source) {
		$this->source = $source;
		return $this;
	}

	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}

	public function setUser($user) {
		$this->user = $user;
		return $this;
	}

}
