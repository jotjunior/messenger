<?php

namespace Messenger\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="msg_messages", indexes={@ORM\Index(name="parent_id", columns={"parent_id"}), @ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity
 */
class Message {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="subject", type="string", length=255, nullable=false)
	 */
	private $subject;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="text", type="text", nullable=false)
	 */
	private $text;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created", type="datetime", nullable=false)
	 */
	private $created;

	/**
	 * @var \MsgMessages
	 *
	 * @ORM\ManyToOne(targetEntity="Message")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
	 * })
	 */
	private $parent;

	/**
	 * @var \UaclUsers
	 *
	 * @ORM\ManyToOne(targetEntity="Uacl\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
	private $user;
	
	public function __construct() {
		$this->created = new \DateTime('now');
	}

	public function getId() {
		return $this->id;
	}

	public function getSubject() {
		return $this->subject;
	}

	public function getText() {
		return $this->text;
	}

	public function getCreated() {
		return $this->created;
	}

	public function getParent() {
		return $this->parent;
	}

	public function getUser() {
		return $this->user;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setSubject($subject) {
		$this->subject = $subject;
		return $this;
	}

	public function setText($text) {
		$this->text = $text;
		return $this;
	}

	public function setCreated($created) {
		$this->created = $created;
		return $this;
	}

	public function setParent($parent) {
		$this->parent = $parent;
		return $this;
	}

	public function setUser($user) {
		$this->user = $user;
		return $this;
	}

}
