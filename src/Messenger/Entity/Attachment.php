<?php

namespace Messenger\Entity; 

use Doctrine\ORM\Mapping as ORM;

/**
 * MsgAttachments
 *
 * @ORM\Table(name="msg_attachments", indexes={@ORM\Index(name="message_id", columns={"message_id"})})
 * @ORM\Entity
 */
class Attachment {

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=255, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="uri", type="string", length=500, nullable=false)
	 */
	private $uri;

	/**
	 * @var \MsgMessages
	 *
	 * @ORM\ManyToOne(targetEntity="Message")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="message_id", referencedColumnName="id")
	 * })
	 */
	private $message;

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getUri() {
		return $this->uri;
	}

	public function getMessage() {
		return $this->message;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function setUri($uri) {
		$this->uri = $uri;
		return $this;
	}

	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}

}
