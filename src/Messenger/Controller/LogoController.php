<?php

namespace Messenger\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class LogoController extends AbstractRestfulController {

	// marca uma mensagem como lida e retorna o código fonte da imagem
	public function getList() {
		// buscando dados da rota
		$data = $this->params()->fromRoute('data', false);
		if (!$data) {
			return new JsonModel(array('success' => 0, 'message' => 'Não foi possível identificar a origem da mensagem'));
		}

		$data = str_replace('_', '=', unserialize(base64_decode($data)));
		if (!is_array($data)) {
			return new JsonModel(array('success' => 0, 'message' => 'Não foi possível extrair os dados da mensagem'));
		}

		// buscando dados da mensagem e usuário
		$em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		$recipient = $em->getRepository('Messenger\Entity\Recipient')->findOneBy(array('user' => $data['userId'], 'message' => $data['messageId']));
		if (!$recipient) {
			return new JsonModel(array('success' => 0, 'message' => 'Não foi possível identificar o destinatário desta mensagem'));
		}

		// gravando a data de leitura da mensagem
		$recipient->setReaded(new \DateTime('now'));
		$em->persist($recipient);
		$em->flush();

		// buscando o CONFIG com os dados da aplicação para exibição da logo
		$config = $this->getServiceLocator()->get('config');

		if (!isset($config['application']) && !isset($config['application']['logo'])) {
			return new JsonModel(array('success' => 0, 'message' => 'Erro ao carregar imagem'));
		}

		// retornando o binário da mensagem
		header("Content-type:image/png");
		echo file_get_contents($config['application']['logo']);
		exit();
	}

}
