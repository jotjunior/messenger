<?php

namespace Messenger\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Doctrine\ORM\EntityManager;

class MessageController extends AbstractRestfulController {

	const ITENS_COUNT_PER_PAGE = 5;

	protected $em;

	public function getList() {

		$user = $this->validateUser();
		if (!($user instanceOf \Uacl\Entity\User)) {
			return $user;
		}

		$recipientRepository = $this->getEm()
				->getRepository('Messenger\Entity\Recipient');

		// lista das mensagens do usuário
		$messagesList = $recipientRepository->findBy(array('user' => $user), array('id' => 'desc'));

		// número de mensagens não lidas
		$messagesUnread = $recipientRepository->countUnreadMessages($user)[0];

		// guardando o número da página atual
		$page = $this->params()->fromQuery('page', 1);

		// instanciando o paginator
		$paginator = new Paginator(new ArrayAdapter($messagesList));
		$paginator->setCurrentPageNumber($page);
		$paginator->setDefaultItemCountPerPage(self::ITENS_COUNT_PER_PAGE);

		// montando a lista de mensagens já paginada
		$listMessage = array();
		foreach ($paginator->getCurrentItems() as $recipient) {
			$listMessage[] = array(
				'messageId' => $recipient->getMessage()->getId(),
				'messageSender' => $recipient->getMessage()->getUser()->getFirstName() . ' ' . $recipient->getMessage()->getUser()->getLastName(),
				'messageSubject' => $recipient->getMessage()->getSubject(),
				'messageDate' => $recipient->getMessage()->getCreated()->format('d/m/Y H:i'),
				'messageLastAccess' => ($recipient->getReaded()) ? $recipient->getReaded()->format('d/m/Y H:i') : '-'
			);
		}

		return new JsonModel(array('unread' => $messagesUnread['unread'], 'messages' => $listMessage));
	}

	public function get($id) {
		$user = $this->validateUser();
		if (!($user instanceOf \Uacl\Entity\User)) {
			return $user;
		}

		// buscando dados da mensagem
		$recipient = $this->getEm()
				->getRepository('Messenger\Entity\Recipient')
				->findOneBy(array('user' => $user, 'message' => $id));

		// alterando a data de leitura da mensagem
		$recipient->setReaded(new \DateTime('now'));
		$this->getEm()->persist($recipient);
		$this->getEm()->flush();

		return new JsonModel(array('message' => $recipient->getMessage()->toArray()));
	}

	/**
	 * Formato do array $data:
	 * [
	 * 		"recipient" => 2, // ID do usuário destinatário
	 * 		"subject" => "Assunto da mensagem",
	 * 		"message" => "Texto da mensagem"
	 * ]
	 * @param array $data
	 * @return \Zend\View\Model\JsonModel
	 */
	public function create($data) {
		$sender = $this->validateUser();

		try {
			$mail = $this->getServiceLocator()->get('Messenger');
			$message = $mail->setData(array('message' => $data['message']))
					->setSubject($data['subject'])
					->setFrom($sender)
					->addTo($this->getEm()->getReference('Uacl\Entity\User', $data['recipient']))
					->saveMessage()
					->send();
			return new JsonModel(array('success' => 1, 'data' => $message));
		} catch (\Exception $e) {
			return new JsonModel(array('success' => 0, 'error' => $e->getMessage(), 'errorCode' => 'EXCEPTION'));
		}
	}

	/**
	 * Retorna o Entity Manger
	 * @return \Doctrine\ORM\EntityManager
	 */
	protected function getEm() {
		if (!($this->em instanceOf EntityManager)) {
			$this->em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
		}
		return $this->em;
	}

	/**
	 * Valida o usuário a partir do token
	 * @return \Zend\View\Model\JsonModel|\Uacl\Entity\User
	 */
	protected function validateUser() {
		// buscando dados do usuário a partir do token
		$server = $this->getServiceLocator()->get('oauth2provider.server.main');
		$token = $server->getAccessTokenData();

		if (!$token) {
			$this->response->setStatusCode(405);
			return new JsonModel(array('success' => false, 'message' => 'Token inválido ou não informado', 'errorCode' => 'INVALID_TOKEN'));
		}

		// busca os dados do usuário a partir do token
		$user = $token['user'];
		if (!$user) {
			$this->response->setStatusCode(405);
			return new JsonModel(array('success' => false, 'message' => 'O Token informado não pertence a nenhum usuário conhecido', 'errorCode' => 'ORPHAN_TOKEN'));
		}

		return $user;
	}

}
