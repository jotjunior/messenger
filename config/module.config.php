<?php

namespace Messenger;

return array(
	'router' => array(
		'routes' => array(
			'route-logo' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/messenger/logo[/:data].png',
					'constraints' => array(
						'data' => '[a-zA-Z0-9/_=]+',
						'controller' => '[a-zA-Z0-9_-]+',
					),
					'defaults' => array(
						'__NAMESPACE__' => 'Messenger\Controller',
						'controller' => 'Logo'
					)
				)
			),
			'route-normal' => array(
				'type' => 'Segment',
				'options' => array(
					'route' => '/api/messenger[/:controller[/:id]]',
					'constraints' => array(
						'id' => '[0-9]+',
						'controller' => '[a-zA-Z0-9_-]+',
					),
					'defaults' => array(
						'__NAMESPACE__' => 'Messenger\Controller',
						'controller' => 'Message'
					)
				)
			)
		)
	),
	'controllers' => array(
		'invokables' => array(
			'Messenger\Controller\Logo' => 'Messenger\Controller\LogoController',
			'Messenger\Controller\Message' => 'Messenger\Controller\MessageController',
			'Messenger\Controller\Recipient' => 'Messenger\Controller\RecipientController',
			'Messenger\Controller\Attachment' => 'Messenger\Controller\AttachmentController',
		),
	),
	'view_manager' => array(
		'strategies' => array(
			'ViewJsonStrategy'
		),
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
	),
	'doctrine' => array(
		'driver' => array(
			__NAMESPACE__ . '_driver' => array(
				'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
				'cache' => 'array',
				'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
			),
			'orm_default' => array(
				'drivers' => array(
					__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
				),
			),
		),
	),
);
